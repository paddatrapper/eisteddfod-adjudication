###########################################################################
# This project is Copyright (C) 2021 Kyle Robbertze <kyle@paddatrapper.com>
#
# This project is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# This project is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this project. If not, see <http://www.gnu.org/licenses/>.
###########################################################################

from dateutil import parser


class Adjudication:
    def __init__(self, row):
        self.date = parser.parse(row[0])
        self.title = row[1]
        self.name = row[2]
        self.school = row[3].strip()
        self.category = row[4].strip()
        self.comments = row[5]
        self.grade_awarded = row[6].strip()
        self.mark = int(row[7].strip())
        self.payment_date = row[8]
        self.payment_amount = row[9]

    def __lt__(self, other):
        return self.mark < other.mark

    def __str__(self):
        return f"{self.name} ({self.school}): {self.grade_awarded}, {self.mark}"
