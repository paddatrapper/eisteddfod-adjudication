###########################################################################
# This project is Copyright (C) 2021 Kyle Robbertze <kyle@paddatrapper.com>
#
# This project is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# This project is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this project. If not, see <http://www.gnu.org/licenses/>.
###########################################################################

import logging
import os

from reportlab.pdfgen.canvas import Canvas
from reportlab.lib.pagesizes import A4, landscape
from reportlab.lib.units import mm
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.platypus import Frame, Paragraph
from reportlab.lib.enums import TA_CENTER
from . import config
from .utils import get_filename, get_base_signature_style


DEBUG = False
PAGE_SIZE = A4
PAGE_WIDTH = PAGE_SIZE[0]
PAGE_HEIGHT = PAGE_SIZE[1]


def get_diploma_canvas(filename):
    canvas = Canvas(filename, pagesize=PAGE_SIZE)
    canvas.setTitle(config.TITLE)
    return canvas


def get_honours_canvas(filename):
    canvas = Canvas(filename, pagesize=landscape(PAGE_SIZE))
    canvas.setTitle(config.TITLE)
    return canvas


def get_name_style(center=True):
    style = getSampleStyleSheet()["Normal"]
    if center:
        style.alignment = TA_CENTER
    style.fontSize = 22
    return style


def get_eisteddfod_style():
    style = getSampleStyleSheet()["Normal"]
    style.fontSize = 18
    return style


def get_year_style():
    style = getSampleStyleSheet()["Normal"]
    style.fontSize = 18
    return style


def get_signature_style():
    style = get_base_signature_style()
    style.fontSize = 18
    return style


class Diploma:
    def __init__(self):
        self.logger = logging.getLogger(__name__)
        self.name_frame = Frame(
            PAGE_WIDTH / 2 - 100 * mm,
            PAGE_HEIGHT / 2 + 53 * mm,
            200 * mm,
            15 * mm,
            showBoundary=DEBUG,
        )
        self.eisteddfod_frame = Frame(
            PAGE_WIDTH / 2, 143 * mm, 50 * mm, 15 * mm, showBoundary=DEBUG
        )
        self.year_frame = Frame(
            PAGE_WIDTH / 2 + 17 * mm, 128 * mm, 50 * mm, 15 * mm, showBoundary=DEBUG
        )
        self.adjudicator_frame = Frame(
            PAGE_WIDTH / 2 + 17 * mm, 96 * mm, 50 * mm, 10 * mm, showBoundary=DEBUG
        )
        self.chairperson_frame = Frame(
            PAGE_WIDTH / 2 + 17 * mm, 78 * mm, 50 * mm, 10 * mm, showBoundary=DEBUG
        )
        self.convenor_frame = Frame(
            PAGE_WIDTH / 2 + 17 * mm, 61 * mm, 50 * mm, 10 * mm, showBoundary=DEBUG
        )

    def generate_pdf(self, adjudication, output_dir):
        filename = get_filename(
            output_dir, adjudication.school, adjudication.name, adjudication.title
        )
        canvas = get_diploma_canvas(filename)
        self.name_frame.addFromList(
            [Paragraph(adjudication.name, get_name_style())], canvas
        )
        self.eisteddfod_frame.addFromList(
            [Paragraph(config.SECTION_SHORT, get_eisteddfod_style())], canvas
        )
        self.year_frame.addFromList(
            [Paragraph(f"<i>{config.YEAR}</i>", get_year_style())], canvas
        )
        self.adjudicator_frame.addFromList(
            [Paragraph(f"<i>{config.ADJUDICATOR_SHORT}</i>", get_signature_style())],
            canvas,
        )
        self.chairperson_frame.addFromList(
            [Paragraph(f"<i>{config.CHAIRPERSON_SHORT}</i>", get_signature_style())],
            canvas,
        )
        self.convenor_frame.addFromList(
            [Paragraph(f"<i>{config.CONVENOR_SHORT}</i>", get_signature_style())],
            canvas,
        )
        canvas.save()


class MeritHonours:
    def __init__(self):
        self.logger = logging.getLogger(__name__)
        self.name_frame = Frame(
            150 * mm, 108 * mm, 130 * mm, 15 * mm, showBoundary=DEBUG
        )
        self.eisteddfod_frame = Frame(
            135 * mm, 72 * mm, 50 * mm, 15 * mm, showBoundary=DEBUG
        )
        self.year_frame = Frame(170 * mm, 55 * mm, 50 * mm, 12 * mm, showBoundary=DEBUG)
        self.chairperson_frame = Frame(
            225 * mm, 45 * mm, 50 * mm, 10 * mm, showBoundary=DEBUG
        )
        self.convenor_frame = Frame(
            225 * mm, 30 * mm, 50 * mm, 10 * mm, showBoundary=DEBUG
        )

    def generate_pdf(self, adjudication, output_dir):
        filename = get_filename(
            output_dir, adjudication.school, adjudication.name, adjudication.title
        )
        canvas = get_honours_canvas(filename)
        self.name_frame.addFromList(
            [Paragraph(adjudication.name, get_name_style(False))], canvas
        )
        self.eisteddfod_frame.addFromList(
            [Paragraph(config.SECTION.upper(), get_eisteddfod_style())], canvas
        )
        self.year_frame.addFromList([Paragraph(config.YEAR, get_year_style())], canvas)
        self.chairperson_frame.addFromList(
            [Paragraph(config.CHAIRPERSON_SHORT, get_signature_style())], canvas
        )
        self.convenor_frame.addFromList(
            [Paragraph(config.CONVENOR_SHORT, get_signature_style())], canvas
        )
        canvas.save()


class Honours(MeritHonours):
    def __init__(self):
        super().__init__()
        self.name_frame = Frame(
            150 * mm, 105 * mm, 130 * mm, 15 * mm, showBoundary=DEBUG
        )
        self.chairperson_frame = Frame(
            210 * mm, 43 * mm, 50 * mm, 10 * mm, showBoundary=DEBUG
        )
        self.convenor_frame = Frame(
            210 * mm, 30 * mm, 50 * mm, 10 * mm, showBoundary=DEBUG
        )


CERTIFICATE_MAP = {
    "diploma": Diploma,
    "high honours": MeritHonours,
    "honours": Honours,
    "high merit": MeritHonours,
    "merit": MeritHonours,
}


def generate_certificate(adjudication, output_dir):
    grade = adjudication.grade_awarded.lower()
    certificate = CERTIFICATE_MAP[grade]()
    certificate.generate_pdf(
        adjudication, os.path.join(output_dir, "certificates", grade)
    )
