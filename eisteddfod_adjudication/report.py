###########################################################################
# This project is Copyright (C) 2021 Kyle Robbertze <kyle@paddatrapper.com>
#
# This project is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# This project is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this project. If not, see <http://www.gnu.org/licenses/>.
###########################################################################

import logging

from reportlab.pdfgen.canvas import Canvas
from reportlab.platypus import Frame, Paragraph, Image
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.units import mm
from reportlab.lib.pagesizes import A5, landscape
from . import config
from .utils import get_filename, get_base_signature_style


DEBUG = False
PAGE_SIZE = landscape(A5)
PAGE_WIDTH = PAGE_SIZE[0]
PAGE_HEIGHT = PAGE_SIZE[1]


def get_canvas(filename):
    canvas = Canvas(filename, pagesize=PAGE_SIZE)
    canvas.setTitle(config.TITLE)
    return canvas


def get_heading_style():
    return getSampleStyleSheet()["Title"]


def get_signature_style():
    style = get_base_signature_style()
    style.fontSize = 24
    return style


class Report:
    def __init__(self):
        self.logger = logging.getLogger(__name__)
        self.title_frame = Frame(
            PAGE_WIDTH / 2 - 100 * mm,
            PAGE_HEIGHT - 15 * mm,
            200 * mm,
            15 * mm,
            showBoundary=DEBUG,
        )
        self.school_frame = Frame(
            5 * mm, 120 * mm, 65 * mm, 10 * mm, showBoundary=DEBUG
        )
        self.art_title_frame = Frame(
            70 * mm, 120 * mm, 70 * mm, 10 * mm, showBoundary=DEBUG
        )
        self.date_frame = Frame(
            140 * mm, 120 * mm, 55 * mm, 10 * mm, showBoundary=DEBUG
        )
        self.name_frame = Frame(
            PAGE_WIDTH / 2 - 40 * mm, 110 * mm, 80 * mm, 10 * mm, showBoundary=DEBUG
        )
        self.comments_frame = Frame(
            10 * mm, 60 * mm, PAGE_WIDTH - 20 * mm, 50 * mm, showBoundary=DEBUG
        )
        self.grade_frame = Frame(
            120 * mm, 40 * mm, 80 * mm, 10 * mm, showBoundary=DEBUG
        )
        self.signature_frame = Frame(
            120 * mm, 10 * mm, 80 * mm, 30 * mm, showBoundary=DEBUG
        )
        self.adjudicator_frame = Frame(
            120 * mm, 5 * mm, 80 * mm, 10 * mm, showBoundary=DEBUG
        )

    def generate_pdf(self, adjudication, output_dir):
        filename = get_filename(
            output_dir, adjudication.school, adjudication.name, adjudication.title
        )
        canvas = get_canvas(filename)
        self.title_frame.addFromList(
            [Paragraph(config.TITLE.upper(), get_heading_style())], canvas
        )
        self.school_frame.addFromList(
            [Paragraph(f"<b>School:</b> {adjudication.school}")], canvas
        )
        self.art_title_frame.addFromList(
            [Paragraph(f"<b>Title:</b> {adjudication.title}")], canvas
        )
        self.date_frame.addFromList(
            [Paragraph(f"<b>Date:</b> {adjudication.date.date()}")], canvas
        )
        self.name_frame.addFromList(
            [Paragraph(f"<b>NAME:</b> {adjudication.name}")], canvas
        )
        self.comments_frame.addFromList(
            [
                Paragraph("<b>ADJUDICATOR'S REPORT:</b>"),
                Paragraph(f"{adjudication.comments}"),
            ],
            canvas,
        )
        self.grade_frame.addFromList(
            [Paragraph(f"<b>GRADE AWARDED:</b> {adjudication.grade_awarded}")], canvas
        )
        self.signature_frame.addFromList(
            [Image("resources/signature.png", width=25 * mm, height=25 * mm)], canvas
        )
        self.adjudicator_frame.addFromList([Paragraph("Adjudicator")], canvas)

        canvas.line(5 * mm, 120 * mm, PAGE_WIDTH - 5 * mm, 120 * mm)
        canvas.line(5 * mm, 110 * mm, PAGE_WIDTH - 5 * mm, 110 * mm)
        canvas.line(120 * mm, 15 * mm, PAGE_WIDTH - 10 * mm, 15 * mm)
        canvas.save()
