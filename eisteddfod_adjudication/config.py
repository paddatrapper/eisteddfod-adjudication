###########################################################################
# This project is Copyright (C) 2021 Kyle Robbertze <kyle@paddatrapper.com>
#
# This project is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# This project is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this project. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
import datetime


TITLE = "Cape Town Art Eisteddfod"
TITLE_SHORT = "Art"
SECTION = "Art Section"
SECTION_SHORT = "Art"
YEAR = datetime.datetime.now().strftime("%Y")
ADJUDICATOR = "Cassian Robbertze"
ADJUDICATOR_SHORT = "C. Robbertze"
CHAIRPERSON_SHORT = "V. Brink"
CONVENOR_SHORT = "T. Robbertze"
