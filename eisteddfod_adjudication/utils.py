###########################################################################
# This project is Copyright (C) 2021 Kyle Robbertze <kyle@paddatrapper.com>
#
# This project is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# This project is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this project. If not, see <http://www.gnu.org/licenses/>.
###########################################################################

import uuid
from pathlib import Path
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.lib.styles import ParagraphStyle
from reportlab.pdfbase.pdfmetrics import registerFont


KEEP_CHARACTERS = (" ", ".", "_")


def sanitize(string):
    clean = [c for c in string if c.isalnum() or c in KEEP_CHARACTERS]
    return "".join(clean).rstrip()


def get_filename(output_dir, school, name, title):
    name = sanitize(name)
    title = sanitize(title)
    school = sanitize(school)
    new_file = Path(output_dir) / school
    new_file.mkdir(parents=True, exist_ok=True)
    new_file = new_file / f"{name}-{title}.pdf"
    if new_file.exists():
        suffix = uuid.uuid4().hex[:5]
        new_file = new_file.parent / f"{name}-{title}-{suffix}.pdf"
    return str(new_file)


def get_base_signature_style():
    registerFont(TTFont("Blackadder ITC", "resources/blackadder_itc.ttf"))
    style = ParagraphStyle(
        name="Signature",
        fontName="Blackadder ITC",
    )
    return style
