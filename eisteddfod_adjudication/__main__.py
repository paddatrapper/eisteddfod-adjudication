###########################################################################
# This project is Copyright (C) 2021 Kyle Robbertze <kyle@paddatrapper.com>
#
# This project is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# This project is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this project. If not, see <http://www.gnu.org/licenses/>.
###########################################################################

import argparse
import csv
import logging
import os
import pprint
from .adjudication import Adjudication
from .report import Report
from .certificate import generate_certificate


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("file", type=open, help="The CSV of the adjudication reports")
    parser.add_argument(
        "-o",
        "--output",
        help="The output directory. Defaults to the current directory",
        default=os.path.abspath("."),
    )
    parser.add_argument(
        "-s",
        "--statistics",
        help="Calculate the statistics of the reports instead of generating the reports and certificates",
        action="store_true",
    )
    parser.add_argument(
        "-v", "--verbose", help="show debug information", action="store_true"
    )
    args = parser.parse_args()
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    logger = logging.getLogger(__name__)
    adjudications = []
    logger.info("Parsing CSV file")
    reader = csv.reader(args.file)
    header = True
    for row in reader:
        # Skip the first row that includes only the header
        if header:
            header = False
            continue
        adjudications.append(Adjudication(row))
    logger.debug(f"Found {len(adjudications)} lines")
    args.file.close()

    if args.statistics:
        logger.info("Calculating statistics")
        school_diplomas = {}
        school_entries = {}
        category_map = {}
        for adjudication in adjudications:
            try:
                category_map[adjudication.category].append(adjudication)
            except KeyError:
                category_map[adjudication.category] = [adjudication]
            if adjudication.grade_awarded.lower() == "diploma":
                try:
                    school_diplomas[adjudication.school] = (
                        school_diplomas[adjudication.school] + 1
                    )
                except KeyError:
                    school_diplomas[adjudication.school] = 1
            try:
                school_entries[adjudication.school] = (
                    school_entries[adjudication.school] + 1
                )
            except KeyError:
                school_entries[adjudication.school] = 1

        for k, v in category_map.items():
            entries = sorted(v, reverse=True)[0:4]
            print(k)
            print([str(x) for x in entries])
            print()

        print("Entries by school:")
        pprint.pprint(school_entries)
        print("\nDiplomas by school:")
        pprint.pprint(school_diplomas)
    else:
        logger.info("Generating PDFs")
        for adjudication in adjudications:
            report = Report()
            report.generate_pdf(adjudication, os.path.join(args.output, "reports"))
            certificate = generate_certificate(adjudication, args.output)
        logger.info(f"PDFs generated. Output in {args.output}")
