## Eisteddfod Report Generator

Generates PDF adjudication reports from CSV.

## Installation

```
sudo apt install fonts-kristi
pipenv install
pipenv run python -m eisteddfod_adjudication -o output file.csv
```
